Welcome to `p-astro`, the LIGO-Virgo low-latency estimator of the probability that a
candidate event belongs to an astrophysical source category.

More information can be found at the review documentation website:
   https://git.ligo.org/shasvath.kapadia/Multicomponent_FGMC_Rates-pAstro

A version of this documentation is included as a PDF file in the docs/ subdirectory.

To install `p-astro` locally:

`python setup.py install --prefix=/path/to/install/directory`

To test:

`python setup.py test`

Documentation:
https://lscsoft.docs.ligo.org/p-astro/